
export function Table() {
  return (
    <div className='layout-column align-items-center justify-content-start'>
      <div className='card pt-30 pb-8 mt-20'>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Age</th>
              <th>Location</th>
              <th>Gender</th>
              <th>Income</th>
            </tr>
          </thead>
          <tbody >
          </tbody>
        </table>
      </div>
    </div>
  )
}


function SearchCustomer() {
  return (
    <>
      <div className='layout-row align-items-center justify-content-center mt-30'>
        <input className='large mx-20 w-20'
          placeholder='Enter search term (Eg: Phil)' />
      </div>
      <Table />
    </>
  )
}

export default SearchCustomer